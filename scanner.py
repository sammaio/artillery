# -*- coding: utf-8 -*-
##
##
#
# Start the nmap scan and wait for it to complete.
# Then format the resonse and send the data back to the server
#
import subprocess
import os
import json




def start_scan():
	'''
	Start the nmap scan of the target
	'''
	process = subprocess.Popen('artillery quick --count 20 -n 40 {0} -o /report.json'.format(os.environ['TARGET']) , shell=True, stdout=subprocess.PIPE)
	process.wait()



def convert_output():
    '''
    Converts the output to json
    '''
    f = open("/report.json","r") 
    d = json.load(f)
    out={}
    out['Type'] = "Artillery"
    out['Host'] = os.environ['TARGET']
    out['aggregate']= d["aggregate"]
    print(json.dumps(out))


start_scan()
convert_output()



