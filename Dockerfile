from ubuntu


#Install artillery
RUN apt-get update && apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update && apt-get install -y nodejs 
RUN npm install -g artillery --unsafe-perm=true --allow-root

COPY scanner.py /scanner.py
RUN chmod +x /scanner.py


CMD  python /scanner.py
